FROM ubuntu:18.04
MAINTAINER Ishan Guliani, https://www.ishanguliani.com
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y  software-properties-common && \
    apt-get update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get install -y net-tools iputils-ping maven gradle nmap wget git vim build-essential && \
    apt-get clean

RUN mkdir /csci652
COPY CSCI652Project1 /csci652/
WORKDIR /csci652
RUN rm -rf target || true
RUN mvn package