package server;

/**
 * A custom exception thrown when Counting Operations fail
 *
 * author: Ishan Guliani (@ig_karmanishth); https://ishanguliani.com
 *
 * version: 1.0
 */
public class CountingFailedException extends Exception {
    public CountingFailedException(String message) {
        super(message);
    }
}
