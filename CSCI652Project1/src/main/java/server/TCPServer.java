package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static helper.Helper.log;

/**
 * Server class inspired from @link{https://https://gitlab.com/ig_karmanishth/distributedsystems/tree/master/socket_programming}
 * This has been modified to received a huge file from multiple clients and perform word counting on them in a
 * multi threaded manner. The results are sent back to the calling client over TCP.
 *
 * author: Ishan Guliani (@ig_karmanishth); https://ishanguliani.com
 *
 * version: 1.0
 */
public class TCPServer {

    public static void main(String[] args) {
        try {
            int serverPort = 7896;
            log("UPDATE  : Trying to set up server socket...");
            ServerSocket listenSocket = new ServerSocket(serverPort);
            log("UPDATE  : success! TCP Server is running and accepting client connections...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                new Connection(clientSocket).start();
            }
        } catch (IOException e) {
            log("Listen :" + e.getMessage());
        }
    }
}

class Connection extends Thread {
    DataInputStream in;
    DataOutputStream out;
    Socket clientSocket;

    public Connection(Socket aClientSocket) {
        try {
            clientSocket = aClientSocket;
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            log("ERROR: server.Connection:" + e.getMessage());
        }
    }

    public void run() {
        try {
            int numberOfBytesToBeReceived = in.readInt();
            log("RECEIVED: The client wants to send: " + numberOfBytesToBeReceived + " bytes of data...");
            byte[] byteBufferArray = new byte[numberOfBytesToBeReceived];
            log("UPDATE  : Beginning reception...");
            in.readFully(byteBufferArray);
            log("RECEIVED: success!");
            String decodedMessage = new String(byteBufferArray, StandardCharsets.UTF_8);
            log("RECEIVED: Here are the first and last few lines of the file sent by the client: "
                    + decodedMessage.substring(0, 60)
                    + "................."
                    + decodedMessage.substring(decodedMessage.length() - 60));
            // processing
            log("UPDATE  : starting the word count process...");
            String processedResults = getProcessedResults(decodedMessage);
            log("UPDATE  : success! Word count process completed!");
            // send result back to client
            int numberOfBytesToSendBackAsResponse = processedResults.getBytes().length;
            log("SEND    : Informing the client about generated response ...");
            out.writeInt(numberOfBytesToSendBackAsResponse);
            log("UPDATE  : success! Informed the client that " + numberOfBytesToSendBackAsResponse + " bytes will be sent back as response!");
            log("SEND    : Sending final response back to the client...");
            out.write(processedResults.getBytes(), 0, numberOfBytesToSendBackAsResponse);
            log("UPDATE  : success! Response sent!");
            log("\nUPDATE  : Waiting for a new client connection...\n\n");
                
        } catch (CountingFailedException e) {
            log("Counting process failed!");
            e.printStackTrace();
        } catch (EOFException e) {
            log("EOF:" + e.getMessage());
        } catch (IOException e) {
            log("IO:" + e.getMessage());
        } finally {
            try {
                clientSocket.close();
            } catch (IOException ignored) {
                // ignoring this exception
            }
        }
    }

    /**
     * Perform word counting on the incomingData by invoking necessary APIs
     * internally and return the String equivalent of the results
     * @param incomingData
     * @return
     * @throws CountingFailedException
     */
    public static String getProcessedResults(String incomingData) throws CountingFailedException {
        WordCount_Seq_Improved wordCountObject = new WordCount_Seq_Improved();
        wordCountObject.performCounting(incomingData);
        if (wordCountObject.hasCountingSucceeded())  {
            return wordCountObject.getCountingResult();
        }   else    {
            throw new CountingFailedException("Could not process word count!");
        }
    }
}
