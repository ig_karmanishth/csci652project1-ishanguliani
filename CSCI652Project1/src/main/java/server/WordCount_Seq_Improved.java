package server;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Word counting process inspired from @link{https://https://gitlab.com/ig_karmanishth/distributedsystems/tree/master/basic_word_count}
 *
 * author: Ishan Guliani (@ig_karmanishth); https://ishanguliani.com
 *
 * version: 1.0
 */
public class WordCount_Seq_Improved {

    private boolean countingSuccess = false;
    private String countingResult = "";

    /**
     * Convert the String data to list of review objects
     * @param incomingData
     * @return  List of AmazonFineFoodReview type
     */
    private List<AmazonFineFoodReview> convertIncomingDataToListOfReviews(String incomingData) {
        List<String> listOfIncomingData = Arrays.asList(incomingData.split("\n"));
        return listOfIncomingData.stream()
                .skip(1)    // skip the header line
                .map(AmazonFineFoodReview::new) // map all remaining lines into an object
                .collect(Collectors.toList());
    }

    /**
     * Get a string equivalent the word count results obtained
     * @param wordcount
     * @return  String type result
     */
    private String getStringRepresentationOfWordCount(Map<String, Integer> wordcount){
        StringBuilder completeString = new StringBuilder();
        for(String word : wordcount.keySet()){
            completeString.append(word).append(" : ").append(wordcount.get(word).toString()).append('\n');
        }
        return completeString.toString();
    }

    /**
     * Emit 1 for every word and store this as a <key, value> pair
     * @param allReviews
     * @return
     */
    private List<KV<String, Integer>> map(List<AmazonFineFoodReview> allReviews) {
        List<KV<String, Integer>> kv_pairs = new ArrayList<KV<String, Integer>>();
        Pattern pattern = Pattern.compile("([a-zA-Z]+)");
        for(AmazonFineFoodReview review : allReviews) {
            if (review.get_Summary() != null)   {
                Matcher matcher = pattern.matcher(review.get_Summary());
                while(matcher.find()) {
                    kv_pairs.add(new KV(matcher.group().toLowerCase(), 1));
                }
            }
        }
        return kv_pairs;
    }
    
    /**
     * count the frequency of each unique word
     * @param kv_pairs
     * @return a list of words with their count
     */
    private Map<String, Integer> reduce(List<KV<String, Integer>> kv_pairs) {
        Map<String, Integer> results = new HashMap<>();

        for(KV<String, Integer> kv : kv_pairs) {
            if(!results.containsKey(kv.getKey())) {
                results.put(kv.getKey(), kv.getValue());
            } else{
                int init_value = results.get(kv.getKey());
                results.replace(kv.getKey(), init_value, init_value+kv.getValue());
            }
        }
        return results;
    }

    public void performCounting(String incomingData) {
        List<AmazonFineFoodReview> allReviews = convertIncomingDataToListOfReviews(incomingData);
        List<KV<String, Integer>> mappedStringToIntegerPairList = map(allReviews);

        Map<String, Integer> reducedStringToIntegerMap = reduce(mappedStringToIntegerPairList);

        // set the results
        setCountingResult(getStringRepresentationOfWordCount(reducedStringToIntegerMap));
        setCountingSuccess(true);
    }

    public boolean hasCountingSucceeded() {
        return countingSuccess;
    }

    private void setCountingSuccess(Boolean countingSuccess)  {
        this.countingSuccess = countingSuccess;
    }

    public String getCountingResult() {
        return countingResult;
    }

    private void setCountingResult(String countingResult) {
        this.countingResult = countingResult;
    }
}
