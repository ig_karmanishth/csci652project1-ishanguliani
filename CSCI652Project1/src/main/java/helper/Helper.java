package helper;

/**
 * A helper class to keep common methods centrally maintained.
 *
 * author: Ishan Guliani (@ig_karmanishth); https://ishanguliani.com
 *
 * version: 1.0
 */
public class Helper {
    public static void log(String message)  {
        System.out.println(message);
    }
}
