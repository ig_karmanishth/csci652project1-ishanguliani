package client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import static helper.Helper.log;

/**
 * TCPClient client class inspired from @link{https://https://gitlab.com/ig_karmanishth/distributedsystems/tree/master/socket_programming}
 * This has been modified to read a huge file and send the contents to a server over a TCP socket. It then received and prints the response
 * sent back by the server
 *
 * author: Ishan Guliani (@ig_karmanishth); https://ishanguliani.com
 *
 * version: 1.0
 */
public class TCPClient {

    private static String RESOURCE_PATH = "src/main/resources/";
    private static String CSV_FILE_TO_READ = "affr.csv";

    public static void main(String args[]) {
        if (args.length < 1)    {
            log("ERROR   : No argument found! Did you miss entering the server `hostname` as argument to this program ?");
            System.exit(0);
        }
        String server_address = args[0];
        Socket tcpSocketToServer = null;
        log("UPDATE  : reading contents of the input file: \"" + CSV_FILE_TO_READ + "\"");
        String stringDataReadFromFile = "";
        try {
            stringDataReadFromFile = readFileAsString(RESOURCE_PATH + CSV_FILE_TO_READ);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        }
        log("UPDATE  : success! File contents read successfully!");
        try {
            int serverPort = 7896;

            log("SEND    : opening socket connection to server...");
            tcpSocketToServer = new Socket(server_address, serverPort);
            log("UPDATE  : success! Connection established successfully!");

            DataInputStream socketInputStream = new DataInputStream(tcpSocketToServer.getInputStream());
            DataOutputStream socketOutputStream = new DataOutputStream(tcpSocketToServer.getOutputStream());

            int numberOfBytesToSendToServer = stringDataReadFromFile.getBytes().length;
            log("SEND    : Informing the server about file content to be sent...");
            socketOutputStream.writeInt(numberOfBytesToSendToServer);
            log("UPDATE  : success! Informed the server that " + numberOfBytesToSendToServer + " bytes will be sent!");

            log("SEND    : Sending file contents...");
            socketOutputStream.write(stringDataReadFromFile.getBytes(), 0, numberOfBytesToSendToServer);
            log("UPDATE  : success! File contents sent successfully!");

            log("UPDATE  : waiting for response...");
            int numberOfBytesToBeSentByServer = socketInputStream.readInt();
            log("RECEIVED: Server wants to send " + numberOfBytesToBeSentByServer + " bytes of response data!");

            log("UPDATE  : waiting for response to be received...");
            // read the response into a byte buffer
            byte[] responseByteBuffer = new byte[numberOfBytesToBeSentByServer];
            socketInputStream.readFully(responseByteBuffer);
            String decodedResponse = new String(responseByteBuffer, StandardCharsets.UTF_8);
            log("RECEIVED: success!");

            log(decodedResponse);

            log("\n Communication is now complete. Please wait for a few seconds. Gracefully exiting...");
            System.exit(0);
        } catch (UnknownHostException e) {
            log("Sock:" + e.getMessage());
        } catch (EOFException e) {
            log("EOF:" + e.getMessage());
        } catch (IOException e) {
            log("IO:" + e.getMessage());
        } finally {
            if (tcpSocketToServer != null)
                try {
                    tcpSocketToServer.close();
                } catch (IOException e) {
                    log("close:" + e.getMessage());
                }
        }
    }

    private static String readFileAsString(String fileToRead) throws FileNotFoundException {
        StringBuilder allFileContents = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(fileToRead))){
            String lineString;
            //read the subsequent lines
            while ((lineString = br.readLine()) != null) {
                allFileContents.append(lineString).append('\n');
            }
        } catch(Exception ignored) {
            throw new FileNotFoundException("Problem reading file!");
        }
        return allFileContents.toString();
    }
}

