# About
 ```
author:         Ishan Guliani (ig_karmanishth)
Components:     1 network (i.e csci652network) and 2 services (i.e client_service, server_service)
Versions:       Docker Desktop 2.1.0.5(40693), Dockerfile v3.7, docker-compose 1.25.2
Docker daemon config(tested on): 4 CPUs, 6 GB Memory, 3 GB Swap

 ```

 # Quick demo
![](https://static.wixstatic.com/media/0a127f_c96acdf1d4544b78a7abc2bc609f4b61~mv2.gif)

 # Description
   - `client_service` depends on `server_service` to run and fire up before itself by using _depends_on_ and _sleep_ commands`
   - Both have their own static _ipv4_addresses_ on the subnet `172.16.238.0/29`
   - The docker file contains pre-compiled code and these services are responsible for executing the program
   - JVM is given _4000MB heap space_ to run without OOMing

# Instructions to fire up containers
Be sure that the address space `172.16.238.0/29` is available and that the docker daemon has enough memory and cpu as [mentioned above](#about) then run:
> `$ docker-compose up --build`

# Note
- Since the _Dockerfile_ is shared, java code compilation happens in the file itself instead of the _docker-compose.yml_ configuration file
- `client_service` will attempt to exit with _status code 0_ once response has been received from the server
- `server_service` will continue to run expecting a new client connection. It needs to be manually killed by pressing `Ctrl+C`

# Common errors
Error:      ```ERROR: Pool overlaps with other one on this address space``` <br>
Solution#1:   Restart the docker daemon <br>
Solution#2:   Run `$ docker network ls` and find the ID of the running instance of network beginning with _csci652network*_. Then remove it with `$ docker network rm <ID>` and try running `$ docker-compose up --build` again <br>
Solution#3:   Update the subnet to a more available subnet in _lines 21, 28, 37 and 52 of docker-compose.yml_